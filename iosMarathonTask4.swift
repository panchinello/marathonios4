enum Answer: String {
	case Yes = "Да"
	case No = "Нет"
}

enum Parameters: Double {
	case Weight = 86.422
	case Hight = 188.42
}

enum Sex {
	case Male
	case Female
}

enum Age: String {
	case Young
	case Middle
	case Elderly
	case Senile
}

enum Experience {
	case Junior
	case Middle
	case Senior
}

enum Colors {
	case Red 
	case Orange 
	case Yellow 
	case Green 
	case Blue 
	case Purple
}

var objects = ["apple", "orange", "sun", "grass", "sea", "berry"]

func colorfullItems (color: Colors) { 
	switch color { 
		case .Red: print("\(objects[0]) red")
		case .Orange: print("\(objects[1]) orange")
		case .Yellow: print("\(objects[2]) yellow")
		case .Green: print("\(objects[3]) green")
		case .Blue: print("\(objects[4]) blue")
		case .Purple: print("\(objects[5]) purple")
	}
}
colorfullItems(color: Colors.Yellow)
colorfullItems(color: Colors.Purple)
colorfullItems(color: Colors.Green)

enum Score: String {
	case A = "A"
	case B = "B"
	case C = "C"
	case D = "D"
	case E = "E"
	case F = "F"
}

func scoreConvertFunc(score: Score) {
	switch score {
		case .A: print(5)
		case .B: print(4)
		case .C: print(3)
		case .D: print(2)
		case .E: print(1)
		case .F: print(0)		
	}
}

scoreConvertFunc(score: Score.A)
scoreConvertFunc(score: Score.C)
scoreConvertFunc(score: Score.F)

enum Cars: String {
	case Bmw = "Bmw"
	case Lada = "Lada"
	case Bently = "Bently"
}

func carsInGarage(cars: [Cars]) {
	for car in cars {
		print("\(car.rawValue)")
	}
}

carsInGarage(cars: [Cars.Bmw, Cars.Lada, Cars.Bently])


